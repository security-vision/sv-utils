import statistics
import networkx as nx
from networkx.algorithms.centrality.betweenness import betweenness_centrality
from networkx.algorithms.centrality.eigenvector import eigenvector_centrality
from pprint import pprint

# from jo
# https://git.rubenvandeven.com/security_vision/securityvisionnr/src/commit/68bc2738998327fa4e8451cb006332a8f294ceb9/data-raw/wiki.R#L145

# mutate(connectivity = tidygraph::centrality_betweenness(),
# connectivity_normalised = 1 + (connectivity-min(connectivity))/sd(connectivity),
# size = connectivity_normalised * if_else(category == "deployments", 2, 1))


def compute_betweenness(nodes, edges):
    G = nx.Graph()
    G.add_nodes_from([(node["id"], node) for node in nodes])
    G.add_edges_from([(edge["from"], edge["to"]) for edge in edges])
    bc = betweenness_centrality(G, k=10, normalized=True, weight=None)

    # bc_values = bc.values()
    # for id, bc_value in bc.items():
    #     bc[id] = 1 + (bc_value - min(bc_values)) / statistics.stdev(bc_values)

    return [{"betweenness": bc[node["id"]], **node} for node in nodes]


# https://networkx.org/documentation/latest/reference/algorithms/generated/networkx.algorithms.centrality.eigenvector_centrality.html#networkx.algorithms.centrality.eigenvector_centrality
def compute_eigenvector(nodes, edges):
    G = nx.Graph()
    G.add_nodes_from([(node["id"], node) for node in nodes])
    G.add_edges_from([(edge["from"], edge["to"]) for edge in edges])
    bc = eigenvector_centrality(G, max_iter=1000, weight=None)

    bc_values = bc.values()
    for id, bc_value in bc.items():
        bc[id] = 1 + (bc_value - min(bc_values)) / statistics.stdev(bc_values)

    return [{"eigenvector": bc[node["id"]], **node} for node in nodes]

import requests, logging, os
from dotenv import load_dotenv
logger = logging.getLogger('session')

def init(url):
    """Initiate a session with the bot credentials for all subsequent queries to the API"""

    # Load bot credentials from an .env file
    logger.info('Loading environmental variables')
    load_dotenv()
    USERNAME = os.environ.get('USERNAME')
    PASSWORD = os.environ.get('PASSWORD')


    logger.info('Initiating a new session')
    session = requests.Session()
    token_query_params = {
        'action':"query",
        'meta': "tokens",
        'type':"login",
        'format':"json"
    }
    
    logger.info('Querying the MediaWiki APi for a one-time login token')
    response = session.get(url, params=token_query_params)
    data = response.json()
    LOGIN_TOKEN = data['query']['tokens']['logintoken']
    
    login_query_params = {
        'action':"login",
        'lgname': USERNAME,
        'lgpassword': PASSWORD,
        'lgtoken': LOGIN_TOKEN,
        'format':"json"  
    }
    
    logger.info('Attempting to login with the environment credential')
    logger.debug(f'USER: {USERNAME}')
    logger.debug(f'PASSWORD: {PASSWORD}')
    logger.debug(f'TOKEN: {LOGIN_TOKEN}')

    response = session.post(url, data=login_query_params)
    data = response.json()

    logger.debug('Login response:')
    logger.debug(data)

    if data['login']['result'] != 'Success':
        raise Exception("Failed loggin in. Try check logs...")

    logger.info('Loggin succeded!')
    
    return session
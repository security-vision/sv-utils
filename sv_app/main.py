from . import session
from . import queries
from pprint import pprint
import os


def delete():
    # Global variables
    base_url = "https://www.securityvision.io/wiki/api.php"
    SESSION = session.init(base_url)

    token_query_params = {
        'action':"query",
        'meta': "tokens",
        'format':"json"
    }

    response = SESSION.get(base_url, params=token_query_params)
    data = response.json()
    LOGIN_TOKEN = data['query']['tokens']['csrftoken']

    category = os.environ.get("CATEGORY", "")
    print(f'Getting pages from category "{category}"')
    pages = queries.get_pages_for_category(category, base_url, SESSION, TEST=False)
    for page in pages:
        result = queries.delete_page(LOGIN_TOKEN, base_url, SESSION, pageid=page['pageid'])
        pprint(result)

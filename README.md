# sv-utils

Interact with the Security Vision wiki. 


## Setup

Install the app using poetry

```
poetry install
```

Be sure to enter your credentials in the .env file!

```
USERNAME=
PASSWORD=
```

## Available scripts

Delete all the pages in a given category

```
export CATEGORY=Test && poetry run delete
```
